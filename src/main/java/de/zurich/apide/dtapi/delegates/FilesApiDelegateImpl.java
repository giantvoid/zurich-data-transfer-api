package de.zurich.apide.dtapi.delegates;

import de.zurich.apide.dtapi.api.FilesApiDelegate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FilesApiDelegateImpl implements FilesApiDelegate {

    @Override
    public ResponseEntity<Void> postFiles(MultipartFile upfile) {
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
