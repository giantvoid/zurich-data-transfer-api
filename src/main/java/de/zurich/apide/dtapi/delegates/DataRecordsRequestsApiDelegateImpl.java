package de.zurich.apide.dtapi.delegates;

import de.zurich.apide.dtapi.Scheduler;
import de.zurich.apide.dtapi.api.DataRecordsRequestsApiDelegate;
import de.zurich.apide.dtapi.model.DataRecordsAsyncRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DataRecordsRequestsApiDelegateImpl implements DataRecordsRequestsApiDelegate {
    private final Scheduler scheduler;

    @Autowired
    public DataRecordsRequestsApiDelegateImpl(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public ResponseEntity<Void> postDataRecordsRequests(DataRecordsAsyncRequest body) {
        scheduler.addScheduledDataRecordsRequest(body);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
