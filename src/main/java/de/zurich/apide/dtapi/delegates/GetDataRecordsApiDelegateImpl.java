package de.zurich.apide.dtapi.delegates;

import de.zurich.apide.dtapi.DataService;
import de.zurich.apide.dtapi.api.GetDataRecordsApiDelegate;
import de.zurich.apide.dtapi.model.DataRecordsRequest;
import de.zurich.apide.dtapi.model.DataRecordsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GetDataRecordsApiDelegateImpl implements GetDataRecordsApiDelegate {

    private final DataService dataService;

    @Autowired
    public GetDataRecordsApiDelegateImpl(DataService dataService) {
        this.dataService = dataService;
    }

    @Override
    public ResponseEntity<DataRecordsResult> getDataRecords(DataRecordsRequest body) {
        DataRecordsResult result = new DataRecordsResult()
                .dataRecordsRequest(body)
                .dataRecords(dataService.getDataRecords(body));

        return ResponseEntity.ok(result);
    }
}
