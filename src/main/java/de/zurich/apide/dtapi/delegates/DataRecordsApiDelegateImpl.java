package de.zurich.apide.dtapi.delegates;

import de.zurich.apide.dtapi.api.DataRecordsApiDelegate;
import de.zurich.apide.dtapi.model.NewDataRecordsContainer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DataRecordsApiDelegateImpl implements DataRecordsApiDelegate {

    @Override
    public ResponseEntity<Void> postDataRecords(NewDataRecordsContainer body) {
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
