package de.zurich.apide.dtapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DtapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtapiApplication.class, args);
    }

}
