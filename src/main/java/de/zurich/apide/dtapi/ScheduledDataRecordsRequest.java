package de.zurich.apide.dtapi;

import de.zurich.apide.dtapi.model.DataRecordsAsyncRequest;
import lombok.Data;

import static de.zurich.apide.dtapi.ScheduledDataRecordsRequest.ExecutionStatus.WAITING;

@Data
public class ScheduledDataRecordsRequest {

    public enum ExecutionStatus {
        WAITING, RUNNING, DONE
    }

    private long nextExecution;
    private ExecutionStatus executionStatus = WAITING;

    private final DataRecordsAsyncRequest dataRecordsAsyncRequest;

}

