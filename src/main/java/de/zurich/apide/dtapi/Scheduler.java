package de.zurich.apide.dtapi;

import de.zurich.apide.dtapi.model.DataRecord;
import de.zurich.apide.dtapi.model.DataRecordsAsyncRequest;
import de.zurich.apide.dtapi.model.DataRecordsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Scheduler {
    private final List<ScheduledDataRecordsRequest> scheduledRequests = new ArrayList<>();

    private final DataService dataService;
    private final RestService restService;

    @Autowired
    public Scheduler(DataService dataService, RestService restService) {
        this.dataService = dataService;
        this.restService = restService;
    }

    @Scheduled(fixedRate = 1000)
    public void scheduledTask() {
        scheduledRequests.forEach(this::triggerScheduledRequest);
        scheduledRequests.removeIf(scheduledRequest -> scheduledRequest.getExecutionStatus() == ScheduledDataRecordsRequest.ExecutionStatus.DONE);
    }

    private void triggerScheduledRequest(ScheduledDataRecordsRequest scheduledRequest) {
        if (scheduledRequest.getExecutionStatus() != ScheduledDataRecordsRequest.ExecutionStatus.WAITING) {
            return;
        }

        if (scheduledRequest.getNextExecution() > System.currentTimeMillis()) {
            return;
        }

        scheduledRequest.setExecutionStatus(ScheduledDataRecordsRequest.ExecutionStatus.RUNNING);
        List<DataRecord> dataRecords = dataService.getDataRecords(scheduledRequest.getDataRecordsAsyncRequest().getDataRecordsRequest());

        DataRecordsResult dataRecordsResult = new DataRecordsResult()
                .dataRecords(dataRecords)
                .dataRecordsRequest(scheduledRequest.getDataRecordsAsyncRequest().getDataRecordsRequest());

        restService.sendDataRecordsResult(scheduledRequest.getDataRecordsAsyncRequest().getResultCallbackUrl(), dataRecordsResult);

        if (scheduledRequest.getDataRecordsAsyncRequest().getExecutionRateMinutes() != null && scheduledRequest.getDataRecordsAsyncRequest().getExecutionRateMinutes() > 0) {
            scheduledRequest.setNextExecution(scheduledRequest.getNextExecution() + scheduledRequest.getDataRecordsAsyncRequest().getExecutionRateMinutes() * 60 * 1000);
            scheduledRequest.setExecutionStatus(ScheduledDataRecordsRequest.ExecutionStatus.WAITING);
        } else {
            scheduledRequest.setExecutionStatus(ScheduledDataRecordsRequest.ExecutionStatus.DONE);
        }

    }

    public void addScheduledDataRecordsRequest(DataRecordsAsyncRequest dataRecordsAsyncRequest) {
        ScheduledDataRecordsRequest scheduledDataRecordsRequest = new ScheduledDataRecordsRequest(dataRecordsAsyncRequest);
        scheduledRequests.add(scheduledDataRecordsRequest);
    }
}
