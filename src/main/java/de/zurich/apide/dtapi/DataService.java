package de.zurich.apide.dtapi;

import de.zurich.apide.dtapi.model.DataRecord;
import de.zurich.apide.dtapi.model.DataRecordsRequest;
import de.zurich.apide.dtapi.model.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataService {
    public List<DataRecord> getDataRecords(DataRecordsRequest dataRecordsRequest) {
        List<DataRecord> result = new ArrayList<>();

        int dataRecordsCount = dataRecordsRequest.getMaxValues() != null && dataRecordsRequest.getMaxValues() > 0 ?
                dataRecordsRequest.getMaxValues() : 1;

        for (int dataRecordIndex = 0; dataRecordIndex < dataRecordsCount; dataRecordIndex++) {
            result.add(createDummyDataRecord(dataRecordIndex + 1));
        }

        return result;
    }

    private DataRecord createDummyDataRecord(int id) {
        return new DataRecord()
                .addValuesItem(new Value().name("id").value(Integer.toString(id)))
                .addValuesItem(new Value().name("name").value("name" + id))
                .addValuesItem(new Value().name("zuersZone").value("zuersZone" + id));
    }

}
